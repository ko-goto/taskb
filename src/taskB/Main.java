package taskB;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {

        System.out.println("入力してください。");

        try(Scanner sc1 = new Scanner(System.in) ){

        	ArrayList<String> results = new ArrayList<String>();
	        // セット数
	        int line1 = Integer.parseInt(sc1.nextLine());

	        for(int i= 0; i < line1; i++) {

	        	String answer = BullsCowsUtils.getAnswer();

	        	// 質問回数
	        	int line2 = Integer.parseInt(sc1.nextLine());

	        	ArrayList<BullsCows> historys = new ArrayList<BullsCows>();

	        	for(int j= 0; j < line2; j++) {

	        		 String question = sc1.nextLine();

	        		 int bulls = BullsCowsUtils.getBulls(answer, question);

	        		 int cows = BullsCowsUtils.getCows(answer, question);

	        		 System.out.println(question + " " + bulls + " " + cows);

	        		 historys.add(new BullsCows(question, bulls, cows));
	        	}

	        	// すべてのパターンを取得する
	        	ArrayList<String> guesses = BullsCowsUtils.getGuessAll();


	        	for(BullsCows history : historys) {


	        		String question = history.getQuestion();

	        		Iterator<String> ite = guesses.iterator();

	        		while (ite.hasNext()) {

	        			String guess = ite.next();

	        			int bulls = BullsCowsUtils.getBulls(question, guess);

	        			int cows = BullsCowsUtils.getCows(question, guess);

	        			// 履歴の bulls & cowsと一致しない場合
	        			if(!history.equals(new BullsCows(guess, bulls, cows))) {
	        				// 削除する
	        				ite.remove();
	        			}
	        		}
		        }

	        	// 答えが一つでない場合
	        	if(guesses.size() != 1) {
	        		results.add("None");
	        	}else {
	        		results.add(guesses.get(0));
	        	}
		    }

	        System.out.println("結果");

	        for(String result : results) {
	        	System.out.println(result);
	        }
        }
	}
}
