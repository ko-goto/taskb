package taskB;

public class BullsCows {

	private final String question;

	private final int bulls;

	private final int cows;

	public BullsCows(String question, int bulls, int cows) {
		this.question = question;
		this.bulls = bulls;
		this.cows = cows;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + bulls;
		result = prime * result + cows;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof BullsCows))
			return false;
		BullsCows other = (BullsCows) obj;
		if (bulls != other.bulls)
			return false;
		if (cows != other.cows)
			return false;
		return true;
	}

	public String getQuestion() {
		return this.question;
	}
}
