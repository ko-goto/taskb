package taskB;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

public class BullsCowsUtils {

	private final static int ANSWER_DIGIT = 4;

	public static String getAnswer() {

		int[] answer = new int[ANSWER_DIGIT];

		for(int i=0; i<answer.length; i++) {

			boolean match;

			do {

				match = false;

				answer[i] = (int) (Math.random() * 10);


				for(int j=0; j < i; j++) {

					if(answer[i] == answer[j]) {
						match = true;
					}
				}

			}while(match == true);

		}
		// int配列からStringに変換
		String result = Arrays.stream(answer)
				.mapToObj(c -> String.valueOf(c))
				.collect(Collectors.joining());

		return result;
	}

	public static int getBulls(String answer , String question) {

		int result = 0;

		for(int i=0; i<answer.length(); i++){

	        char a = answer.charAt(i);
	        char q = question.charAt(i);

	        if(a == q) {
	        	result++;
	        }
		}
		return result;
	}

	public static int getCows(String answer , String question) {

		int result = 0;

		for(int i=0; i<answer.length(); i++){

	        char q = question.charAt(i);

	        if(answer.contains(String.valueOf(q))) {
	        	result++;
	        }
		}
		return result;
	}

	public static ArrayList<String> getGuessAll() {

		int size = (int) Math.pow(10, ANSWER_DIGIT);

		ArrayList<String> result = new ArrayList<String>();

		for( int i = 0; i < size; i++ ) {
			result.add(String.format( "%0" + ANSWER_DIGIT + "d ", i));
		}

		return result;

	}
}
